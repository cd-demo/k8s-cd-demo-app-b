const os = require('os')
const express = require('express')
const app = express()
 
const PORT = process.env.PORT || 3001

app.get('/bar', function (req, res, next) {
    res.send('bar')
})

app.get('/hello', function (req, res, next) {
    res.send('hello world')
})

app.get('/host', function (req, res, next) {
    res.send(os.hostname())
})

app.listen(PORT)

console.info(`Server up and running on port ${PORT}`)