FROM ubuntu:latest

RUN apt-get update && apt-get upgrade -y && apt-get install -y curl gnupg gnupg1 gnupg2
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -y nodejs

ADD . app
WORKDIR app

CMD node index.js
